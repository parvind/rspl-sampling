<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>RSPL</title>
        <link href="../assets/css/bootstrap.css" rel="stylesheet" />
        <link href="../assets/css/style.css" rel="stylesheet" />
    </head>
    <body class="bg-light-pink">
        <div class="login-page">

            <div class="left-box">
                <div class="left-box-inner">
                    <img src="../assets/images/white-logo.png" alt="Image">
                    <div class="profile-pic">
                        <img src="../assets/images/white-profile.png" alt="Image">
                    </div>
                    <h4>Lorem ipsum dolor</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipi incididunt ut labore et dolore magna aliqua.</p>
                </div>
            </div>

            <div class="login-form-box">
                <div class="login-form-inner">

                    <h2>Reset Password</h2>

                    <form>
                        <div class="input-box">
                            <label>New Password</label>
                            <input type="password" placeholder="**********" value="" name="name" required />
                        </div>
                        <div class="input-box">
                            <label>Confirm Password</label>
                            <input type="text" placeholder="" value="" name="name" required />
                        </div>
                        <div class="login-button">
                            <input type="submit" value="Reset" />
                        </div>
                    </form>

                </div>

            </div>
        </div>
    </body>
</html>
