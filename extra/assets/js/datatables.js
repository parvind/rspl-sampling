// Call the dataTables jQuery plugin
$(document).ready(function() {
  $('#dataTableSampleStats').DataTable({

    dom: 'Bfrtip',
    buttons: [
    { extend: 'csv', text: 'Export' }
    ]

  });
});

