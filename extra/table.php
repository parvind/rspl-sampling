<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>RSPL</title>
        <link href="../assets/css/bootstrap.css" rel="stylesheet" />
        <link href="../assets/css/style.css" rel="stylesheet" />
        <link href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css" rel="stylesheet" crossorigin="anonymous" />
        <link href="https://cdn.datatables.net/select/1.4.0/css/select.dataTables.min.css" rel="stylesheet" crossorigin="anonymous" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/js/all.min.js" crossorigin="anonymous"></script>
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-blue">
            <a class="navbar-brand" href="index.php"><img src="../assets/images/white-logo.png" alt="Logo"></a>
            <button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#"><i class="fas fa-bars"></i></button>
            <!-- Navbar-->
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="user-img-small"><img src="../assets/images/profile-2.png" alt="img"></i>
                        Sampling Department
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                        <!-- <div class="dropdown-divider"></div> -->
                        <a class="dropdown-item" href="login.php">Logout</a>
                    </div>
                </li>
            </ul>
        </nav>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion bg-blue" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">

                            <div class="profile-box">
                                <img src="../assets/images/profile.png" alt="profile">
                                <span class="profile-name">Wade Warren</span>
                            </div>

                            <a class="nav-link dashboard active" href="dashboard.php">Dashboard</a>
                            <a class="nav-link stats" href="#">Stats Report</a>
                            <a class="nav-link sample" href="#">Sample Detail</a>
                            <a class="nav-link result" href="#">Sample Results</a>
                            <a class="nav-link notification" href="#">Notifications</a>
                            <a class="nav-link logout" href="#">Logout</a>

                        </div>
                    </div>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">Dashboard</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Client Main Dash Board</li>
                        </ol>
                        <!--  -->
                        <div class="row">
                            <div class="col-xl-6">

                                <form class="filter-form dis-flex">
                                    <div class="input-box flex-2">
                                        <input type="text" name="product" placeholder="Product Name">
                                    </div>
                                    <div class="input-box flex-2">
                                        <input type="text" name="product" placeholder="Product Type">
                                    </div>
                                    <div class="input-box flex-2">
                                        <input type="text" name="product" placeholder="Date In" onfocus="(this.type='date')">
                                    </div>
                                    <div class="input-box flex-2">
                                        <input type="text" name="product" placeholder="End Date" onfocus="(this.type='date')">
                                    </div>
                                    <div class="input-box flex-2">
                                        <input type="text" name="product" placeholder="Date Out" onfocus="(this.type='date')">
                                    </div>
                                    <div class="input-box flex-2">
                                        <input type="text" name="product" placeholder="Status">
                                    </div>
                                    <div class="input-box flex-2">
                                        <input type="text" name="product" placeholder="Case Status">
                                    </div>
                                    <div class="input-box flex-2">
                                        <input type="text" name="product" placeholder="BCode">
                                    </div>
                                    <div class="input-box flex-2">
                                        <button type="submit" value="Submit" class="btn-default btn-green">Filter</button>
                                    </div>
                                    <div class="input-box flex-2">
                                      <button type="reset" value="Reset" class="btn-default btn-blue">Reset</button>
                                    </div>
                                </form>


                            </div>
                            <div class="col-xl-6">
                                <form class="search-record-form">
                                    <div class="dis-flex nowrap">
                                        <div class="input-box flex-20">
                                            <select name="search">
                                                <option value="account">Account Number</option>
                                                <option value="column">Any Column</option>
                                                <option value="bcode">BCode</option>
                                                <option value="status">Case Status</option>
                                                <option value="care">Customer Care</option>
                                                <option value="in">Date In</option>
                                                <option value="details">Details</option>
                                                <option value="name">Name</option>
                                                <option value="id">ID</option>
                                                <option value="employee">List by Employee</option>
                                                <option value="type">Product Type</option>
                                                <option value="remarks">Remarks</option>
                                                <option value="requirement">Requirement</option>
                                                <option value="name">Product Name</option>
                                                <option value="status">Status</option>
                                                <option value="out">Date Out</option>
                                            </select>
                                        </div>
                                        <div class="input-box flex-60">
                                            <input type="text" name="product" placeholder="Full Search">
                                        </div>

                                        <div class="input-box flex-20">
                                          <button type="search" value="Reset" class="btn-default btn-yellow">Search</button>
                                        </div>
                                    </div>
                                </form>

                                <form class="add-record-form">
                                    <div class="dis-flex">
                                        <div class="input-box flex-20">
                                            <select name="search">
                                                <option value="tracker">Tracker</option>
                                                <option value="get">Get New Data</option>
                                                <option value="refresh">Refresh Tracker</option>
                                            </select>
                                        </div>
                                        <div class="input-box flex-20">
                                            <select name="search">
                                                <option value="filter">Filter</option>
                                                <option value="dateout">Todays Dateout</option>
                                                <option value="refresh">Refresh Tracker</option>
                                            </select>
                                        </div>
                                        <div class="input-box flex-20">
                                            <select name="search">
                                                <option value="pivot">Select Pivot</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="input-box">
                                      <button type="submit" value="Reset" class="btn-default btn-yellow save">Save</button>
                                    </div>
                                    <div class="input-box">
                                      <button type="submit" value="Reset" class="btn-default btn-yellow">Add New Record</button>
                                    </div>
                                    <div class="input-box">
                                      <button type="submit" value="Reset" class="btn-default btn-yellow">Add Recheck Record</button>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <!--  -->
                        <div class="card mb-4 mt-5">
                            <div class="card-header">
                                <i class="fas fa-table mr-1"></i>
                                Added New Cases fields
                            </div>
                            <div class="card-body">
                                <form class="add-new-case-field dis-flex">
                                    <div class="label-box flex-40">
                                        <label>Column Name</label>
                                    </div>
                                    <div class="input-box flex-60">
                                        <input type="text" name="product" placeholder="Value">
                                    </div>

                                    <div class="label-box flex-40">
                                        <label>Name</label>
                                    </div>
                                    <div class="input-box flex-60">
                                        <input type="text" name="product">
                                    </div>
                                    <div class="label-box flex-40">
                                        <label>Account Number</label>
                                    </div>
                                    <div class="input-box flex-60">
                                        <input type="text" name="product">
                                    </div>
                                    <div class="label-box flex-40">
                                        <label>Update Product Name</label>
                                    </div>
                                    <div class="input-box flex-60">
                                        <input type="text" name="product">
                                    </div>
                                    <div class="label-box flex-40">
                                        <label>Update Requirement</label>
                                    </div>
                                    <div class="input-box flex-60">
                                        <input type="text" name="product">
                                    </div>
                                    <div class="label-box flex-40">
                                        <label>Client Code</label>
                                    </div>
                                    <div class="input-box flex-60">
                                        <input type="text" name="product">
                                    </div>
                                    <div class="label-box flex-40">
                                        <label>APPID</label>
                                    </div>
                                    <div class="input-box flex-60">
                                        <input type="text" name="product">
                                    </div>

                                   <div class="button-box">
                                        <button type="submit" value="Submit" class="btn-default btn-green">Save</button>
                                    </div>
                                    <div class="button-box">
                                      <button type="reset" value="Reset" class="btn-default btn-blue">Reset</button>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <!--  -->

                        <div class="card mb-4 mt-5">
                            <div class="card-header">
                                <i class="fas fa-table mr-1"></i>
                                Product Name
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="display" id="dataTable" width="100%" cellspacing="0">
                                        
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>PRODUT NAME</th>
                                                <th>UPDATED PRODUCT NAME</th>
                                                <th>CORRECT UPN</th>
                                                <th>PRODUCT TYPE</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td></td>
                                                <td>AADHAAR CARD</td>
                                                <td>AADHAAR CARD</td>
                                                <td>UID</td>
                                                <td>KYC</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>AIRTEL</td>
                                                <td>AIRTEL</td>
                                                <td>ATL</td>
                                                <td>MOBILE</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>ALLAHABAD ACCOUNT SEARCH</td>
                                                <td>ALLAHABAD ACCOUNT SEARCH</td>
                                                <td>IND</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>ALLAHABAD BANK</td>
                                                <td>ALLAHABAD BANK / INDIAN BANK</td>
                                                <td>IDN</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>ALLAHABAD PAN SEARCH</td>
                                                <td>ALLAHABAD PAN SEARCH</td>
                                                <td>IND</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>ALLAHABAD STATEMENT</td>
                                                <td>ALLAHABAD STATEMENT</td>
                                                <td>IDNS</td>
                                                <td>STATEMENT</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>ALLAHABAD TRACK</td>
                                                <td>ALLAHABAD BANK / INDIAN BANK</td>
                                                <td>IDN</td>
                                                <td>LOAN TRACK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>ANDHRA ACCOUNT SEARCH</td>
                                                <td>ANDHRA ACCOUNT SEARCH</td>
                                                <td>UBI</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>ANDHRA BANK</td>
                                                <td>UNION BANK OF INDIA / CORPORATION / ANDHRA</td>
                                                <td>UBI</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>ANDHRA CARD</td>
                                                <td>ANDHRA CARD</td>
                                                <td>UBI</td>
                                                <td>CREDIT CARD</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>ANDHRA PAN SEARCH</td>
                                                <td>ANDHRA PAN SEARCH</td>
                                                <td>UBI</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>ANDHRA STATEMENT</td>
                                                <td>ANDHRA STATEMENT</td>
                                                <td>UBIS</td>
                                                <td>STATEMENT</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>APD</td>
                                                <td>APD</td>
                                                <td>APD</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>AXIS ACCOUNT SEARCH</td>
                                                <td>AXIS ACCOUNT SEARCH</td>
                                                <td>XSB</td>
                                                <td>PVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>AXIS BANK</td>
                                                <td>AXIS BANK</td>
                                                <td>XSB</td>
                                                <td>PVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>AXIS CARD</td>
                                                <td>AXIS CARD</td>
                                                <td>XSB</td>
                                                <td>CREDIT CARD</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>AXIS PAN SEARCH</td>
                                                <td>AXIS PAN SEARCH</td>
                                                <td>XSB</td>
                                                <td>PVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>AXIS STATEMENT</td>
                                                <td>AXIS STATEMENT</td>
                                                <td>XSBS</td>
                                                <td>STATEMENT</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>AXIS TRACK</td>
                                                <td>AXIS BANK</td>
                                                <td>XSB</td>
                                                <td>LOAN TRACK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>BANDHAN BANK</td>
                                                <td>BANDHAN BANK</td>
                                                <td>BND</td>
                                                <td>PVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>BANK OF MAHARASHTRA</td>
                                                <td>BANK OF MAHARASHTRA</td>
                                                <td>BOM</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>BANK OF MAHARASHTRA ACCOUNT SEARCH</td>
                                                <td>BANK OF MAHARASHTRA ACCOUNT SEARCH</td>
                                                <td>BOM</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>BANK OF MAHARASHTRA PAN SEARCH</td>
                                                <td>BANK OF MAHARASHTRA PAN SEARCH</td>
                                                <td>BOM</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>BOB ACCOUNT SEARCH</td>
                                                <td>BOB ACCOUNT SEARCH</td>
                                                <td>BOB</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>BOB BANK</td>
                                                <td>BANK OF BARODA / VIJAYA / DENA</td>
                                                <td>BOB</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>BOB CARD</td>
                                                <td>BOB CARD</td>
                                                <td>BOB</td>
                                                <td>CREDIT CARD</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>BOB PAN SEARCH</td>
                                                <td>BOB PAN SEARCH</td>
                                                <td>BOB</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>BOB STATEMENT</td>
                                                <td>BOB STATEMENT</td>
                                                <td>BOBS</td>
                                                <td>STATEMENT</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>BOB TRACK</td>
                                                <td>BANK OF BARODA / VIJAYA / DENA</td>
                                                <td>BOB</td>
                                                <td>LOAN TRACK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>BOI ACCOUNT SEARCH</td>
                                                <td>BOI ACCOUNT SEARCH</td>
                                                <td>BOI</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>BOI BANK</td>
                                                <td>BOI BANK</td>
                                                <td>BOI</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>BOI CARD</td>
                                                <td>BOI CARD</td>
                                                <td>BOI</td>
                                                <td>CREDIT CARD</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>BOI PAN SEARCH</td>
                                                <td>BOI PAN SEARCH</td>
                                                <td>BOI</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>BOI STATEMENT</td>
                                                <td>BOI STATEMENT</td>
                                                <td>BOIS</td>
                                                <td>STATEMENT</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>BOI TRACK</td>
                                                <td>BOI BANK</td>
                                                <td>BOI</td>
                                                <td>LOAN TRACK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>BOM CARD</td>
                                                <td>BOM CARD</td>
                                                <td>BOM</td>
                                                <td>CREDIT CARD</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>BOM STATEMENT</td>
                                                <td>BANK OF MAHARASHTRA</td>
                                                <td>BOMS</td>
                                                <td>STATEMENT</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>BSNL</td>
                                                <td>BSNL</td>
                                                <td>BSNL</td>
                                                <td>MOBILE</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>CANARA ACCOUNT SEARCH</td>
                                                <td>CANARA ACCOUNT SEARCH</td>
                                                <td>CNR</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>CANARA BANK</td>
                                                <td>SYNDICATE BANK / CANARA BANK</td>
                                                <td>CNR</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>CANARA CARD</td>
                                                <td>CANARA CARD</td>
                                                <td>CNR</td>
                                                <td>CREDIT CARD</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>CANARA PAN SEARCH</td>
                                                <td>CANARA PAN SEARCH</td>
                                                <td>CNR</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>CANARA STATEMENT</td>
                                                <td>CANARA STATEMENT</td>
                                                <td>CNRS</td>
                                                <td>STATEMENT</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>CANARA TRACK</td>
                                                <td>SYNDICATE BANK / CANARA BANK</td>
                                                <td>CNR</td>
                                                <td>LOAN TRACK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>CBI ACCOUNT SEARCH</td>
                                                <td>CBI ACCOUNT SEARCH</td>
                                                <td>CBI</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>CBI BANK</td>
                                                <td>CBI BANK</td>
                                                <td>CBI</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>CBI CARD</td>
                                                <td>CBI CARD</td>
                                                <td>CBI</td>
                                                <td>CREDIT CARD</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>CBI PAN SEARCH</td>
                                                <td>CBI PAN SEARCH</td>
                                                <td>CBI</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>CBI STATEMENT</td>
                                                <td>CBI STATEMENT</td>
                                                <td>CBIS</td>
                                                <td>STATEMENT</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>CBI TRACK</td>
                                                <td>CBI BANK</td>
                                                <td>CBI</td>
                                                <td>LOAN TRACK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>CIBIL</td>
                                                <td>CIBIL</td>
                                                <td>CIBIL</td>
                                                <td>KYC</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>CITI BANK</td>
                                                <td>CITI BANK</td>
                                                <td>CITI</td>
                                                <td>PVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>CITI CARD</td>
                                                <td>CITI CARD</td>
                                                <td>CITI</td>
                                                <td>CREDIT CARD</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>CITI STATEMENT</td>
                                                <td>CITI STATEMENT</td>
                                                <td>CITS</td>
                                                <td>STATEMENT</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>CITI TRACK</td>
                                                <td>CITI BANK</td>
                                                <td>CITI</td>
                                                <td>LOAN TRACK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>CITI UNION BANK</td>
                                                <td>CITI UNION BANK</td>
                                                <td>CUB</td>
                                                <td>PVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>CITI UNION CARD</td>
                                                <td>CITI UNION CARD</td>
                                                <td>CUB</td>
                                                <td>CREDIT CARD</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>CITI UNION TRACK</td>
                                                <td>CITI UNION BANK</td>
                                                <td>CUB</td>
                                                <td>LOAN TRACK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>CORPORATION ACCOUNT SEARCH</td>
                                                <td>CORPORATION ACCOUNT SEARCH</td>
                                                <td>CORP</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>CORPORATION BANK</td>
                                                <td>UNION BANK OF INDIA / CORPORATION / ANDHRA</td>
                                                <td>UBI</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>CORPORATION CARD</td>
                                                <td>CORPORATION CARD</td>
                                                <td>UBI</td>
                                                <td>CREDIT CARD</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>CORPORATION PAN SEARCH</td>
                                                <td>CORPORATION PAN SEARCH</td>
                                                <td>CORP</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>CORPORATION STATEMENT</td>
                                                <td>CORPORATION STATEMENT</td>
                                                <td>UBIS</td>
                                                <td>STATEMENT</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>CORPORATION TRACK</td>
                                                <td>UNION BANK OF INDIA / CORPORATION / ANDHRA</td>
                                                <td>UBI</td>
                                                <td>LOAN TRACK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>COSMOS BANK</td>
                                                <td>COSMOS BANK</td>
                                                <td>CMB</td>
                                                <td>PVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>DENA ACCOUNT SEARCH</td>
                                                <td>DENA ACCOUNT SEARCH</td>
                                                <td>BOB</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>DENA BANK</td>
                                                <td>BANK OF BARODA / VIJAYA / DENA</td>
                                                <td>BOB</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>DENA CARD</td>
                                                <td>DENA CARD</td>
                                                <td>BOB</td>
                                                <td>CREDIT CARD</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>DENA PAN SEARCH</td>
                                                <td>DENA PAN SEARCH</td>
                                                <td>BOB</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>DENA STATEMENT</td>
                                                <td>DENA STATEMENT</td>
                                                <td>BOBS</td>
                                                <td>STATEMENT</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>DENA TRACK</td>
                                                <td>BANK OF BARODA / VIJAYA / DENA</td>
                                                <td>BOB</td>
                                                <td>LOAN TRACK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>DEUTSCHE BANK</td>
                                                <td>DEUTSCHE BANK</td>
                                                <td>DTB</td>
                                                <td>PVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>DEUTSCHE CARD</td>
                                                <td>DEUTSCHE CARD</td>
                                                <td>DTB</td>
                                                <td>CREDIT CARD</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>DEUTSCHE STATEMENT</td>
                                                <td>DEUTSCHE STATEMENT</td>
                                                <td>DTBS</td>
                                                <td>STATEMENT</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>DUAL PAN CARD</td>
                                                <td>PAN CARD</td>
                                                <td>DP</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FEDERAL ACCOUNT SEARCH</td>
                                                <td>FEDERAL ACCOUNT SEARCH</td>
                                                <td>FEDS</td>
                                                <td>PVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FEDERAL BANK</td>
                                                <td>FEDERAL BANK</td>
                                                <td>FDRL</td>
                                                <td>PVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FEDERAL PAN SEARCH</td>
                                                <td>FEDERAL PAN SEARCH</td>
                                                <td>FEDS</td>
                                                <td>PVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FEDERAL STATEMENT</td>
                                                <td>FEDERAL STATEMENT</td>
                                                <td>FEDS</td>
                                                <td>STATEMENT</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FIN 2000-01</td>
                                                <td>FINANCIALS</td>
                                                <td>FIN</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FIN 2001-02</td>
                                                <td>FINANCIALS</td>
                                                <td>FIN</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FIN 2002-03</td>
                                                <td>FINANCIALS</td>
                                                <td>FIN</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FIN 2003-04</td>
                                                <td>FINANCIALS</td>
                                                <td>FIN</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FIN 2004-05</td>
                                                <td>FINANCIALS</td>
                                                <td>FIN</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FIN 2005-06</td>
                                                <td>FINANCIALS</td>
                                                <td>FIN</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FIN 2007-08</td>
                                                <td>FINANCIALS</td>
                                                <td>FIN</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FIN 2008-09</td>
                                                <td>FINANCIALS</td>
                                                <td>FIN</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FIN 2009-10</td>
                                                <td>FINANCIALS</td>
                                                <td>FIN</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FIN 2010-11</td>
                                                <td>FINANCIALS</td>
                                                <td>FIN</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FIN 2011-12</td>
                                                <td>FINANCIALS</td>
                                                <td>FIN</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FIN 2012-13</td>
                                                <td>FINANCIALS</td>
                                                <td>FIN</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FIN 2013-14</td>
                                                <td>FINANCIALS</td>
                                                <td>FIN</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FIN 2014-15</td>
                                                <td>FINANCIALS</td>
                                                <td>FIN</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FIN 2015-16</td>
                                                <td>FINANCIALS</td>
                                                <td>FIN</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FIN 2016-17</td>
                                                <td>FINANCIALS</td>
                                                <td>FIN</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FIN 2017-18</td>
                                                <td>FINANCIALS</td>
                                                <td>FIN</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FIN 2018-19</td>
                                                <td>FINANCIALS</td>
                                                <td>FIN</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FIN 2019-20</td>
                                                <td>FINANCIALS</td>
                                                <td>FIN</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FIN 2020-21</td>
                                                <td>FINANCIALS</td>
                                                <td>FIN</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FIN 2021-22</td>
                                                <td>FINANCIALS</td>
                                                <td>FIN</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FIN 2022-23</td>
                                                <td>FINANCIALS</td>
                                                <td>FIN</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FIN 2023-24</td>
                                                <td>FINANCIALS</td>
                                                <td>FIN</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FORM 26AS 2000-01</td>
                                                <td>FORM 26</td>
                                                <td>26</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FORM 26AS 2001-02</td>
                                                <td>FORM 26</td>
                                                <td>26</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FORM 26AS 2002-03</td>
                                                <td>FORM 26</td>
                                                <td>26</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FORM 26AS 2003-04</td>
                                                <td>FORM 26</td>
                                                <td>26</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FORM 26AS 2004-05</td>
                                                <td>FORM 26</td>
                                                <td>26</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FORM 26AS 2005-06</td>
                                                <td>FORM 26</td>
                                                <td>26</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FORM 26AS 2006-07</td>
                                                <td>FORM 26</td>
                                                <td>26</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FORM 26AS 2007-08</td>
                                                <td>FORM 26</td>
                                                <td>26</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FORM 26AS 2008-09</td>
                                                <td>FORM 26</td>
                                                <td>26</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FORM 26AS 2009-10</td>
                                                <td>FORM 26</td>
                                                <td>26</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FORM 26AS 2010-11</td>
                                                <td>FORM 26</td>
                                                <td>26</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FORM 26AS 2011-12</td>
                                                <td>FORM 26</td>
                                                <td>26</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FORM 26AS 2012-13</td>
                                                <td>FORM 26</td>
                                                <td>26</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FORM 26AS 2013-14</td>
                                                <td>FORM 26</td>
                                                <td>26</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FORM 26AS 2014-15</td>
                                                <td>FORM 26</td>
                                                <td>26</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FORM 26AS 2015-16</td>
                                                <td>FORM 26</td>
                                                <td>26</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FORM 26AS 2016-17</td>
                                                <td>FORM 26</td>
                                                <td>26</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FORM 26AS 2017-18</td>
                                                <td>FORM 26</td>
                                                <td>26</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FORM 26AS 2018-19</td>
                                                <td>FORM 26</td>
                                                <td>26</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FORM 26AS 2019-20</td>
                                                <td>FORM 26</td>
                                                <td>26</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FORM 26AS 2020-21</td>
                                                <td>FORM 26</td>
                                                <td>26</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FORM 26AS 2021-22</td>
                                                <td>FORM 26</td>
                                                <td>26</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FORM 26AS 2022-23</td>
                                                <td>FORM 26</td>
                                                <td>26</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>FORM 26AS 2023-24</td>
                                                <td>FORM 26</td>
                                                <td>26</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>GST</td>
                                                <td>GST</td>
                                                <td>GST</td>
                                                <td>KYC</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>GSTR-1</td>
                                                <td>GSTR-1</td>
                                                <td>GST</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>HDFC ACCOUNT SEARCH</td>
                                                <td>HDFC ACCOUNT SEARCH</td>
                                                <td>HDF</td>
                                                <td>PVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>HDFC BANK</td>
                                                <td>HDFC BANK</td>
                                                <td>HDF</td>
                                                <td>PVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>HDFC CARD</td>
                                                <td>HDFC CARD</td>
                                                <td>HDF</td>
                                                <td>CREDIT CARD</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>HDFC PAN SEARCH</td>
                                                <td>HDFC PAN SEARCH</td>
                                                <td>HDF</td>
                                                <td>PVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>HDFC STATEMENT</td>
                                                <td>HDFC STATEMENT</td>
                                                <td>HDFS</td>
                                                <td>STATEMENT</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>HDFC TRACK</td>
                                                <td>HDFC TRACK</td>
                                                <td>HDF</td>
                                                <td>LOAN TRACK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>HSBC BANK</td>
                                                <td>HSBC BANK</td>
                                                <td>HBC</td>
                                                <td>PVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>HSBC CARD</td>
                                                <td>HSBC CARD</td>
                                                <td>HBC</td>
                                                <td>CREDIT CARD</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>HSBC STATEMENT</td>
                                                <td>HSBC STATEMENT</td>
                                                <td>HBCS</td>
                                                <td>STATEMENT</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>HSBC TRACK</td>
                                                <td>HSBC BANK</td>
                                                <td>HBC</td>
                                                <td>LOAN TRACK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>ICICI ACCOUNT SEARCH</td>
                                                <td>ICICI ACCOUNT SEARCH</td>
                                                <td>ICI</td>
                                                <td>PVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>ICICI BANK</td>
                                                <td>ICICI BANK</td>
                                                <td>ICI</td>
                                                <td>PVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>ICICI CARD</td>
                                                <td>ICICI CARD</td>
                                                <td>ICI</td>
                                                <td>CREDIT CARD</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>ICICI PAN SEARCH</td>
                                                <td>ICICI PAN SEARCH</td>
                                                <td>ICI</td>
                                                <td>PVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>ICICI STATEMENT</td>
                                                <td>ICICI STATEMENT</td>
                                                <td>ICIS</td>
                                                <td>STATEMENT</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>ICICI TRACK</td>
                                                <td>ICICI BANK</td>
                                                <td>ICI</td>
                                                <td>LOAN TRACK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>IDBI ACCOUNT SEARCH</td>
                                                <td>IDBI ACCOUNT SEARCH</td>
                                                <td>IDB</td>
                                                <td>PVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>IDBI BANK</td>
                                                <td>IDBI BANK</td>
                                                <td>IDB</td>
                                                <td>PVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>IDBI CARD</td>
                                                <td>IDBI CARD</td>
                                                <td>IDB</td>
                                                <td>CREDIT CARD</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>IDBI PAN SEARCH</td>
                                                <td>IDBI PAN SEARCH</td>
                                                <td>IDB</td>
                                                <td>PVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>IDBI STATEMENT</td>
                                                <td>IDBI STATEMENT</td>
                                                <td>IDBS</td>
                                                <td>STATEMENT</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>IDBI TRACK</td>
                                                <td>IDBI BANK</td>
                                                <td>IDB</td>
                                                <td>LOAN TRACK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>IDFC ACCOUNT SEARCH</td>
                                                <td>IDFC ACCOUNT SEARCH</td>
                                                <td>IDF</td>
                                                <td>PVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>IDFC BANK</td>
                                                <td>IDFC BANK</td>
                                                <td>IDFC</td>
                                                <td>PVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>IDFC PAN SEARCH</td>
                                                <td>IDFC PAN SEARCH</td>
                                                <td>IDF</td>
                                                <td>PVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>IDFC STATEMENT</td>
                                                <td>IDFC STATEMENT</td>
                                                <td>IDFS</td>
                                                <td>STATEMENT</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>IDFC TRACK</td>
                                                <td>IDFC BANK</td>
                                                <td>IDFC</td>
                                                <td>LOAN TRACK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>INDIAN ACCOUNT SEARCH</td>
                                                <td>INDIAN ACCOUNT SEARCH</td>
                                                <td>IDN</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>INDIAN BANK</td>
                                                <td>ALLAHABAD BANK / INDIAN BANK</td>
                                                <td>IDN</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>INDIAN PAN SEARCH</td>
                                                <td>INDIAN PAN SEARCH</td>
                                                <td>IDN</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>INDIAN STATEMENT</td>
                                                <td>INDIAN STATEMENT</td>
                                                <td>IDNS</td>
                                                <td>STATEMENT</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>INDIAN TRACK</td>
                                                <td>ALLAHABAD BANK / INDIAN BANK</td>
                                                <td>IDN</td>
                                                <td>LOAN TRACK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>INDUSIND ACCOUNT SEARCH</td>
                                                <td>INDUSIND ACCOUNT SEARCH</td>
                                                <td>IND</td>
                                                <td>PVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>INDUSIND BANK</td>
                                                <td>INDUSIND BANK</td>
                                                <td>IND</td>
                                                <td>PVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>INDUSIND CARD</td>
                                                <td>INDUSIND CARD</td>
                                                <td>IND</td>
                                                <td>CREDIT CARD</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>INDUSIND PAN SEARCH</td>
                                                <td>INDUSIND PAN SEARCH</td>
                                                <td>IND</td>
                                                <td>PVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>INDUSIND STATEMENT</td>
                                                <td>INDUSIND STATEMENT</td>
                                                <td>INDS</td>
                                                <td>STATEMENT</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>INDUSIND TRACK</td>
                                                <td>INDUSIND BANK</td>
                                                <td>IND</td>
                                                <td>LOAN TRACK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>IOB ACCOUNT SEARCH</td>
                                                <td>IOB ACCOUNT SEARCH</td>
                                                <td>IOB</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>IOB BANK</td>
                                                <td>IOB BANK</td>
                                                <td>IOB</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>IOB CARD</td>
                                                <td>IOB CARD</td>
                                                <td>IOB</td>
                                                <td>CREDIT CARD</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>IOB PAN SEARCH</td>
                                                <td>IOB PAN SEARCH</td>
                                                <td>IOB</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>IOB STATEMENT</td>
                                                <td>IOB STATEMENT</td>
                                                <td>IOBS</td>
                                                <td>STATEMENT</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>IOB TRACK</td>
                                                <td>IOB BANK</td>
                                                <td>IOB</td>
                                                <td>LOAN TRACK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>ITR 1997-98</td>
                                                <td>INCOME TAX RETURN</td>
                                                <td>ITR</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>ITR 2001-02</td>
                                                <td>INCOME TAX RETURN</td>
                                                <td>ITR</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>ITR 2002-03</td>
                                                <td>INCOME TAX RETURN</td>
                                                <td>ITR</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>ITR 2003-04</td>
                                                <td>INCOME TAX RETURN</td>
                                                <td>ITR</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>ITR 2004-05</td>
                                                <td>INCOME TAX RETURN</td>
                                                <td>ITR</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>ITR 2005-06</td>
                                                <td>INCOME TAX RETURN</td>
                                                <td>ITR</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>ITR 2006-07</td>
                                                <td>INCOME TAX RETURN</td>
                                                <td>ITR</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>ITR 2007-08</td>
                                                <td>INCOME TAX RETURN</td>
                                                <td>ITR</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>ITR 2008-09</td>
                                                <td>INCOME TAX RETURN</td>
                                                <td>ITR</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>ITR 2009-10</td>
                                                <td>INCOME TAX RETURN</td>
                                                <td>ITR</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>ITR 2010-11</td>
                                                <td>INCOME TAX RETURN</td>
                                                <td>ITR</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>ITR 2011-12</td>
                                                <td>INCOME TAX RETURN</td>
                                                <td>ITR</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>ITR 2012-13</td>
                                                <td>INCOME TAX RETURN</td>
                                                <td>ITR</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>ITR 2013-14</td>
                                                <td>INCOME TAX RETURN</td>
                                                <td>ITR</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>ITR 2014-15</td>
                                                <td>INCOME TAX RETURN</td>
                                                <td>ITR</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>ITR 2015-16</td>
                                                <td>INCOME TAX RETURN</td>
                                                <td>ITR</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>ITR 2016-17</td>
                                                <td>INCOME TAX RETURN</td>
                                                <td>ITR</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>ITR 2017-18</td>
                                                <td>INCOME TAX RETURN</td>
                                                <td>ITR</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>ITR 2018-19</td>
                                                <td>INCOME TAX RETURN</td>
                                                <td>ITR</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>ITR 2019-20</td>
                                                <td>INCOME TAX RETURN</td>
                                                <td>ITR</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>ITR 2020-21</td>
                                                <td>INCOME TAX RETURN</td>
                                                <td>ITR</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>ITR 2021-22</td>
                                                <td>INCOME TAX RETURN</td>
                                                <td>ITR</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>ITR 2022-23</td>
                                                <td>INCOME TAX RETURN</td>
                                                <td>ITR</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>ITR 2023-24</td>
                                                <td>INCOME TAX RETURN</td>
                                                <td>ITR</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>JIO</td>
                                                <td>JIO</td>
                                                <td>JIO</td>
                                                <td>MOBILE</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>KARANTAKA STATEMENT</td>
                                                <td>KARANTAKA STATEMENT</td>
                                                <td>KNTS</td>
                                                <td>STATEMENT</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>KARNATAKA BANK</td>
                                                <td>KARNATAKA BANK</td>
                                                <td>KNT</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>KARUR VYSYA BANK</td>
                                                <td>KARUR VYSYA BANK</td>
                                                <td>KVB</td>
                                                <td>PVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>KOTAK BANK</td>
                                                <td>KOTAK BANK</td>
                                                <td>KTK</td>
                                                <td>PVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>KOTAK CARD</td>
                                                <td>KOTAK CARD</td>
                                                <td>KTK</td>
                                                <td>CREDIT CARD</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>KOTAK STATEMENT</td>
                                                <td>KOTAK BANK</td>
                                                <td>KTKS</td>
                                                <td>STATEMENT</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>KOTAK TRACK</td>
                                                <td>KOTAK BANK</td>
                                                <td>KTK</td>
                                                <td>LOAN TRACK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>KVB BANK</td>
                                                <td>KARUR VYSYA BANK</td>
                                                <td>KVB</td>
                                                <td>PVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>KVB BANK</td>
                                                <td>KARUR VYSYA BANK</td>
                                                <td>KVB</td>
                                                <td>PVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>KVB STATEMENT</td>
                                                <td>KVB STATEMENT</td>
                                                <td>KVBS</td>
                                                <td>STATEMENT</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>LAKSHMI VILAS BANK</td>
                                                <td>LAKSHMI VILAS BANK</td>
                                                <td>LVB</td>
                                                <td>PVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>LVB BANK</td>
                                                <td>LAKSHMI VILAS BANK</td>
                                                <td>LVB</td>
                                                <td>PVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>LVB STATEMENT</td>
                                                <td>LVB STATEMENT</td>
                                                <td>LVBS</td>
                                                <td>STATEMENT</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>OBC ACCOUNT SEARCH</td>
                                                <td>OBC ACCOUNT SEARCH</td>
                                                <td>PNB</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>OBC BANK</td>
                                                <td>OBC BANK / PNB BANK / UNITED BANK OF INDIA</td>
                                                <td>PNB</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>OBC CARD</td>
                                                <td>OBC CARD</td>
                                                <td>PNB</td>
                                                <td>CREDIT CARD</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>OBC PAN SEARCH</td>
                                                <td>OBC PAN SEARCH</td>
                                                <td>PNB</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>OBC STATEMENT</td>
                                                <td>OBC STATEMENT</td>
                                                <td>PNBS</td>
                                                <td>STATEMENT</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>OBC TRACK</td>
                                                <td>OBC BANK / PNB BANK / UNITED BANK OF INDIA</td>
                                                <td>PNB</td>
                                                <td>LOAN TRACK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>PAN CARD</td>
                                                <td>PAN CARD</td>
                                                <td>PAN</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>PAN INVESTIGATION</td>
                                                <td>PAN INVESTIGATION</td>
                                                <td>PN</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>PAN WITH PHOTO</td>
                                                <td>PAN WITH PHOTO</td>
                                                <td>PAR</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>PASSPORT</td>
                                                <td>PASSPORT</td>
                                                <td>PAS</td>
                                                <td>KYC</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>PF</td>
                                                <td>PF</td>
                                                <td>PF</td>
                                                <td>KYC</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>PNB ACCOUNT SEARCH</td>
                                                <td>PNB ACCOUNT SEARCH</td>
                                                <td>PNB</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>PNB BANK</td>
                                                <td>OBC BANK / PNB BANK / UNITED BANK OF INDIA</td>
                                                <td>PNB</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>PNB CARD</td>
                                                <td>PNB CARD</td>
                                                <td>PNB</td>
                                                <td>CREDIT CARD</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>PNB PAN SEARCH</td>
                                                <td>PNB PAN SEARCH</td>
                                                <td>PNB</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>PNB statement</td>
                                                <td>PNB STATEMENT</td>
                                                <td>PNBS</td>
                                                <td>STATEMENT</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>PNB TRACK</td>
                                                <td>OBC BANK / PNB BANK / UNITED BANK OF INDIA</td>
                                                <td>PNB</td>
                                                <td>LOAN TRACK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>PSB BANK</td>
                                                <td>PUNJAB & SINDH BANK</td>
                                                <td>PSB</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>PSB CARD</td>
                                                <td>PSB CARD</td>
                                                <td>PSB</td>
                                                <td>CREDIT CARD</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>PSB STATEMENT</td>
                                                <td>PSB STATEMENT</td>
                                                <td>PSBS</td>
                                                <td>STATEMENT</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>PSB TRACK</td>
                                                <td>PUNJAB & SINDH BANK</td>
                                                <td>PSB</td>
                                                <td>LOAN TRACK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>RBL BANK</td>
                                                <td>RBL BANK</td>
                                                <td>RBL</td>
                                                <td>PVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>RBL CARD</td>
                                                <td>RBL CARD</td>
                                                <td>RBL</td>
                                                <td>CREDIT CARD</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>RBL STATEMENT</td>
                                                <td>RBL STATEMENT</td>
                                                <td>RBLS</td>
                                                <td>STATEMENT</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>RBL TRACK</td>
                                                <td>RBL BANK</td>
                                                <td>RBL</td>
                                                <td>LOAN TRACK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>REVISED ITR 2013-14</td>
                                                <td>INCOME TAX RETURN</td>
                                                <td>ITR</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>REVISED ITR 2014-15</td>
                                                <td>INCOME TAX RETURN</td>
                                                <td>ITR</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>REVISED ITR 2015-16</td>
                                                <td>INCOME TAX RETURN</td>
                                                <td>ITR</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>REVISED ITR 2016-17</td>
                                                <td>INCOME TAX RETURN</td>
                                                <td>ITR</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>REVISED ITR 2017-18</td>
                                                <td>INCOME TAX RETURN</td>
                                                <td>ITR</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>REVISED ITR 2018-19</td>
                                                <td>INCOME TAX RETURN</td>
                                                <td>ITR</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>REVISED ITR 2019-20</td>
                                                <td>INCOME TAX RETURN</td>
                                                <td>ITR</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>REVISED ITR 2020-21</td>
                                                <td>INCOME TAX RETURN</td>
                                                <td>ITR</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>REVISED ITR 2021-22</td>
                                                <td>INCOME TAX RETURN</td>
                                                <td>ITR</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>REVISED ITR 2022-23</td>
                                                <td>INCOME TAX RETURN</td>
                                                <td>ITR</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>REVISED ITR 2023-24</td>
                                                <td>INCOME TAX RETURN</td>
                                                <td>ITR</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>REVISED ITR 2024-25</td>
                                                <td>INCOME TAX RETURN</td>
                                                <td>ITR</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>REVISED ITR 2025-26</td>
                                                <td>INCOME TAX RETURN</td>
                                                <td>ITR</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>REVISED ITR 2026-27</td>
                                                <td>INCOME TAX RETURN</td>
                                                <td>ITR</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>REVISED ITR 2027-28</td>
                                                <td>INCOME TAX RETURN</td>
                                                <td>ITR</td>
                                                <td>ITO</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>SARASWAT BANK</td>
                                                <td>SARASWAT BANK</td>
                                                <td>SWT</td>
                                                <td>PVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>SARASWAT STATEMENT</td>
                                                <td>SARASWAT STATEMENT</td>
                                                <td>SWTS</td>
                                                <td>STATEMENT</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>SARASWAT TRACK</td>
                                                <td>SARASWAT BANK</td>
                                                <td>SWT</td>
                                                <td>LOAN TRACK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>SBI ACCOUNT SEARCH</td>
                                                <td>SBI ACCOUNT SEARCH</td>
                                                <td>SBI</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>SBI BANK</td>
                                                <td>SBI BANK</td>
                                                <td>SBI</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>SBI CARD</td>
                                                <td>SBI CARD</td>
                                                <td>SBI</td>
                                                <td>CREDIT CARD</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>SBI PAN SEARCH</td>
                                                <td>SBI PAN SEARCH</td>
                                                <td>SBI</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>SBI STATEMENT</td>
                                                <td>SBI STATEMENT</td>
                                                <td>SBIS</td>
                                                <td>STATEMENT</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>SBI TRACK</td>
                                                <td>SBI BANK</td>
                                                <td>SBI</td>
                                                <td>LOAN TRACK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>SCB BANK</td>
                                                <td>SCB BANK</td>
                                                <td>SCB</td>
                                                <td>PVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>SCB CARD</td>
                                                <td>SCB CARD</td>
                                                <td>SCB</td>
                                                <td>CREDIT CARD</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>SCB STATEMENT</td>
                                                <td>SCB STATEMENT</td>
                                                <td>SCBS</td>
                                                <td>STATEMENT</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>SCB TRACK</td>
                                                <td>SCB BANK</td>
                                                <td>SCB</td>
                                                <td>LOAN TRACK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>SIB STATEMENT</td>
                                                <td>SIB STATEMENT</td>
                                                <td>SIBS</td>
                                                <td>STATEMENT</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>SOUTH INDIAN BANK</td>
                                                <td>SOUTH INDIAN BANK</td>
                                                <td>SIB</td>
                                                <td>PVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>SYNDICATE ACCOUNT SEARCH</td>
                                                <td>SYNDICATE ACCOUNT SEARCH</td>
                                                <td>CNR</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>SYNDICATE BANK</td>
                                                <td>SYNDICATE BANK / CANARA BANK</td>
                                                <td>CNR</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>SYNDICATE CARD</td>
                                                <td>SYNDICATE CARD</td>
                                                <td>CNR</td>
                                                <td>CREDIT CARD</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>SYNDICATE PAN SEARCH</td>
                                                <td>SYNDICATE PAN SEARCH</td>
                                                <td>CNR</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>SYNDICATE STATEMENT</td>
                                                <td>SYNDICATE STATEMENT</td>
                                                <td>CNRS</td>
                                                <td>STATEMENT</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>TATA</td>
                                                <td>TATA</td>
                                                <td>TATA</td>
                                                <td>MOBILE</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>UAN</td>
                                                <td>PF</td>
                                                <td>UAN</td>
                                                <td>KYC</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>UBI ACCOUNT SEARCH</td>
                                                <td>UBI ACCOUNT SEARCH</td>
                                                <td>UBI</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>UBI BANK</td>
                                                <td>UNION BANK OF INDIA / CORPORATION / ANDHRA</td>
                                                <td>UBI</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>UBI CARD</td>
                                                <td>UBI CARD</td>
                                                <td>UBI</td>
                                                <td>CREDIT CARD</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>UBI PAN SEARCH</td>
                                                <td>UBI PAN SEARCH</td>
                                                <td>UBI</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>UBI STATEMENT</td>
                                                <td>UBI STATEMENT</td>
                                                <td>UBIS</td>
                                                <td>STATEMENT</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>UBI TRACK</td>
                                                <td>UNION BANK OF INDIA / CORPORATION / ANDHRA</td>
                                                <td>UBI</td>
                                                <td>LOAN TRACK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>UCO BANK</td>
                                                <td>UCO BANK</td>
                                                <td>UCO</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>UCO STATEMENT</td>
                                                <td>UCO STATEMENT</td>
                                                <td>UCOS</td>
                                                <td>STATEMENT</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>UCO TRACK</td>
                                                <td>UCO BANK</td>
                                                <td>UCO</td>
                                                <td>LOAN TRACK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>UNITED BANK OF INDIA</td>
                                                <td>OBC BANK / PNB BANK / UNITED BANK OF INDIA</td>
                                                <td>PNB</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>UNITED BANK OF INDIA</td>
                                                <td>OBC BANK / PNB BANK / UNITED BANK OF INDIA</td>
                                                <td>PNB</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>UNITED BANK OF INDIA STATEMENT</td>
                                                <td>UNITED BANK OF INDIA STATEMENT</td>
                                                <td>PNBS</td>
                                                <td>STATEMENT</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>UNITED OF INDIA ACCOUNT SEARCH</td>
                                                <td>UNITED OF INDIA ACCOUNT SEARCH</td>
                                                <td>PNB</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>UNITED OF INDIA PAN SEARCH</td>
                                                <td>UNITED OF INDIA PAN SEARCH</td>
                                                <td>PNB</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>VODAPHONE</td>
                                                <td>VI</td>
                                                <td>VI</td>
                                                <td>MOBILE</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>VIJAYA BANK</td>
                                                <td>BANK OF BARODA / VIJAYA / DENA</td>
                                                <td>BOB</td>
                                                <td>GOVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>VIJAYA CARD</td>
                                                <td>VIJAYA CARD</td>
                                                <td>BOB</td>
                                                <td>CREDIT CARD</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>VIJAYA STATEMENT</td>
                                                <td>VIJAYA STATEMENT</td>
                                                <td>BOBS</td>
                                                <td>STATEMENT</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>VOTER ID</td>
                                                <td>VOTER ID</td>
                                                <td>VID</td>
                                                <td>KYC</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>YES ACCOUNT SEARCH</td>
                                                <td>YES ACCOUNT SEARCH</td>
                                                <td>YES</td>
                                                <td>PVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>YES BANK</td>
                                                <td>YES BANK</td>
                                                <td>YES</td>
                                                <td>PVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>YES CARD</td>
                                                <td>YES CARD</td>
                                                <td>YES</td>
                                                <td>CREDIT CARD</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>YES PAN SEARCH</td>
                                                <td>YES PAN SEARCH</td>
                                                <td>YES</td>
                                                <td>PVT BANK</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>YES STATEMENT</td>
                                                <td>YES STATEMENT</td>
                                                <td>YES</td>
                                                <td>STATEMENT</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>



                        <div class="card mb-4 mt-5">
                            <div class="card-header">
                                <i class="fas fa-table mr-1"></i>
                                Trackers
                            </div>
                            <div class="card-body">
                                <div class="all-traker-show dis-flex">
                                    <div class="label-box flex-70">
                                        <label><strong>Tracker Name</strong></label>
                                    </div>
                                    <div class="button-box flex-30">
                                        <span><strong>Action</strong></span>
                                    </div>



                                    <div class="label-box flex-70">
                                        <label>A P Associates(AP)</label>
                                    </div>
                                    <div class="button-box flex-30">
                                        <button class="btn-default btn-blue">Open</button>
                                    </div>
                                    <div class="label-box flex-70">
                                        <label>Additional Tracker</label>
                                    </div>
                                    <div class="button-box flex-30">
                                        <button class="btn-default btn-blue">Open</button>
                                    </div>
                                    <div class="label-box flex-70">
                                        <label>Agencies Dedupe Tracker</label>
                                    </div>
                                    <div class="button-box flex-30">
                                        <button class="btn-default btn-blue">Open</button>
                                    </div>
                                    <div class="label-box flex-70">
                                        <label>Agencies Dedupe Tracker(SADAP)</label>
                                    </div>
                                    <div class="button-box flex-30">
                                        <button class="btn-default btn-blue">Open</button>
                                    </div>
                                    <div class="label-box flex-70">
                                        <label>Agencies Dedupe Tracker(ITO)</label>
                                    </div>
                                    <div class="button-box flex-30">
                                        <button class="btn-default btn-blue">Open</button>
                                    </div>
                                    <div class="label-box flex-70">
                                        <label>Agencies Cost Testing</label>
                                    </div>
                                    <div class="button-box flex-30">
                                        <button class="btn-default btn-blue">Open</button>
                                    </div>
                                    <div class="label-box flex-70">
                                        <label>AHM Services(HM)</label>
                                    </div>
                                    <div class="button-box flex-30">
                                        <button class="btn-default btn-blue">Open</button>
                                    </div>
                                    <div class="label-box flex-70">
                                        <label>Allocation MIS</label>
                                    </div>
                                    <div class="button-box flex-30">
                                        <button class="btn-default btn-blue">Open</button>
                                    </div>
                                    <div class="label-box flex-70">
                                        <label>Aslam Pivot</label>
                                    </div>
                                    <div class="button-box flex-30">
                                        <button class="btn-default btn-blue">Open</button>
                                    </div>
                                    <div class="label-box flex-70">
                                        <label>ASP Astute</label>
                                    </div>
                                    <div class="button-box flex-30">
                                        <button class="btn-default btn-blue">Open</button>
                                    </div>
 

                                </div>
                            </div>
                        </div>



                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; 2022</div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.5.1.js" crossorigin="anonymous"></script>
        <script src="../assets/js/bootstrap.js"></script>
        <script src="../assets/js/script.js"></script>
        <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.datatables.net/select/1.4.0/js/dataTables.select.min.js" crossorigin="anonymous"></script>
        <!-- <script src="../assets/demo/datatables-demo.js"></script> -->
    </body>
</html>
