// Call the dataTables jQuery plugin
$(document).ready(function() {
  $('#dataTableSampleStats').DataTable({

    dom: 'Bfrtip',
    buttons: [
    { extend: 'csv', text: 'Export' }
    ]

  });


  $('#dataTable').DataTable( {
    dom: 'Bfrtip',
    columnDefs: [ {
        orderable: false,
        className: 'select-checkbox',
        targets:   0,
        paging: false,
        ordering: false,
        info: false
    } ],
    select: {
        style:    'os',
        selector: 'td:first-child'
    },
    order: [[ 1, 'asc' ]]
} );



});

