// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontColor = '#858796';

// Pie Chart Example
var ctx = document.getElementById("sampleStateChart");
var myPieChart = new Chart(ctx, {
  type: 'doughnut',
  data: {
    labels: ["Pass", "Failed", "In Progress"],
    datasets: [{
      data: [45, 35, 20],
      backgroundColor: ['rgba(138, 60, 172, 0.1)', '#095BA9', '#022A50'],
      hoverBackgroundColor: ['#2e59d9', '#2e59d9', '#2e59d9'],
      hoverBorderColor: "rgba(234, 236, 244, 1)",
    }],
  },
  options: {
    maintainAspectRatio: true,
    tooltips: {
      backgroundColor: "rgb(255,255,255)",
      bodyFontColor: "#858796",
      borderColor: '#dddfeb',
      borderWidth: 1,
      xPadding: 15,
      yPadding: 15,
      displayColors: true,
      caretPadding: 10,
    },
    legend: {
      display: true,
      position: 'bottom',
    },
    cutoutPercentage: 50,
  },
});
