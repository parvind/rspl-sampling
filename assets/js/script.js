
(function($) {
    "use strict";

    // Add active state to sidbar nav links
    var path = window.location.href; // because the 'href' property of the DOM element is the absolute path
        // $("#layoutSidenav_nav .sb-sidenav a.nav-link").each(function() {
        //     if (this.href === path) {
        //         $(this).addClass("active");
        //     }
        // });

    // Toggle the side navigation
    $("#sidebarToggle").on("click", function(e) {
        e.preventDefault();
        $("body").toggleClass("sb-sidenav-toggled");
    });
})(jQuery);

// $(document).ready(function() {
//   $('#dataTable').DataTable();
// });



let fBtn = document.querySelector('.filter-btn');
fBtn.onclick = function(){
    let fOpen = document.querySelector('.sample-filter-box-outer');
    fOpen.classList.toggle('d-block')
};

let close = document.querySelector('.dashboard-container .close-btn');
close.onclick = function(e) {
    let hHide = document.querySelector('.sample-filter-box-outer');
    hHide.classList.remove('d-block');
    e.preventDefault();
}





