// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#292b2c';


// Area Chart Example
var ctx = document.getElementById("labSampleStatusChart");
var myLineChart = new Chart(ctx, {
  type: 'line',
  data: {
    labels: ["May 2022", "Jun 2022", "July 2022", "Aug 2022", "Sep 2022", "Oct 2022", "Nov 2022", "Dec 2022"],
    datasets: [
      {
      label: "Failed: ",
      lineTension: 0.3,
      backgroundColor: "rgba(2,117,216,0)",
      borderColor: "#DADADA",
      pointRadius: 5,
      pointBackgroundColor: "#DADADA",
      pointBorderColor: "rgba(255,255,255,0.8)",
      pointHoverRadius: 5,
      pointHoverBackgroundColor: "#DADADA",
      pointHitRadius: 50,
      pointBorderWidth: 2,
      data: [50, 250, 200, 300, 350, 200, 300, 350]
    },

    {
      label: "Pass: ",
      lineTension: 0.3,
      backgroundColor: "rgba(2,117,216,0)",
      borderColor: "#022A50",
      pointRadius: 5,
      pointBackgroundColor: "#022A50",
      pointBorderColor: "rgba(255,255,255,0.8)",
      pointHoverRadius: 5,
      pointHoverBackgroundColor: "#022A50",
      pointHitRadius: 50,
      pointBorderWidth: 2,
      data: [100, 200, 240, 100, 150, 250, 340, 300]
    },

    {
      label: "In Progress: ",
      lineTension: 0.3,
      backgroundColor: "rgba(2,117,216,0)",
      borderColor: "#1665D8",
      pointRadius: 5,
      pointBackgroundColor: "#1665D8",
      pointBorderColor: "rgba(255,255,255,0.8)",
      pointHoverRadius: 5,
      pointHoverBackgroundColor: "#1665D8",
      pointHitRadius: 50,
      pointBorderWidth: 2,
      data: [140, 250, 200, 150, 100, 200, 300, 300]
    }

  ],
  },

  options: {
    scales: {
      xAxes: [{
        time: {
          unit: 'date'
        },
        gridLines: {
          display: false
        },
        ticks: {
          maxTicksLimit: 8
        }
      }],
      yAxes: [{
        ticks: {
          min: 0,
          max: 400,
          maxTicksLimit: 5
        },
        gridLines: {
          color: "rgba(2, 42, 80, 0.1)",
        }
      }],
    },
    legend: {
      display: true,
      position: 'bottom',
    }
  }
});
